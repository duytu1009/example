import { Component, OnInit } from '@angular/core';
import { DanhMucMonModel } from '../shared/models/danh-muc-mon.model';
import { DanhMucMonService } from '../shared/services/danh-muc-mon.service';

@Component({
  selector: 'app-ban-hang',
  templateUrl: './ban-hang.component.html',
  styleUrls: ['./ban-hang.component.css']
})
export class BanHangComponent implements OnInit {

  constructor(public service: DanhMucMonService) { }
  lstMon: Array<DanhMucMonModel> = [
    {
      maMon: 1,
      tenMon:'Trà đào',
      maLoai:3,
      donGia: 25000

    },
    {
      maMon: 2,
      tenMon:'Trà xanh đá xay',
      maLoai:4,
      donGia: 35000
    },
    {
      maMon: 3,
      tenMon:'Trà sữa trà đen',
      maLoai:1,
      donGia: 28000
    }
  ];
  ngOnInit(): void {
  }

}
