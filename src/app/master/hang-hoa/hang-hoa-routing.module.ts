import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { HangHoaComponent } from './hang-hoa.component';
import { DanhMucMonAddComponent } from './danh-muc-mon/danh-muc-mon-add/danh-muc-mon-add.component';
import { DanhMucMonListComponent } from './danh-muc-mon/danh-muc-mon-list/danh-muc-mon-list.component';
import { MasterComponent } from '../master.component';
import { DanhMucNVLListComponent } from './danh-muc-nvl/danh-muc-nvl-list/danh-muc-nvl-list.component';
import { DanhMucNVLAddComponent } from './danh-muc-nvl/danh-muc-nvl-add/danh-muc-nvl-add.component';
import { ThietLapGiaListComponent } from './thiet-lap-gia/thiet-lap-gia-list/thiet-lap-gia-list.component';
import { DanhMucNVLEditComponent } from './danh-muc-nvl/danh-muc-nvl-edit/danh-muc-nvl-edit.component';
import { DanhMucMonEditComponent } from './danh-muc-mon/danh-muc-mon-edit/danh-muc-mon-edit.component';

const routes: Routes = [
  { path: 'danhmucmon/list', component: DanhMucMonListComponent },
  { path: 'danhmucmon/add', component: DanhMucMonAddComponent },
  { path: 'danhmucmon/:maMon/edit', component: DanhMucMonEditComponent},


  { path: 'danhmucNVL/list', component: DanhMucNVLListComponent },
  { path: 'danhmucNVL/add', component: DanhMucNVLAddComponent },
  // { path: 'danhmucNVL/edit', component: DanhMucNVLEditComponent },
  {path: 'danhmucNVL/:maNVL/edit', component: DanhMucNVLEditComponent},

  { path: 'thietlapgia/list', component: ThietLapGiaListComponent }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class HangHoaRoutingModule { }
