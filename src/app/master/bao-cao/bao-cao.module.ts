import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { BaoCaoCuoiNgayComponent } from './bao-cao-cuoi-ngay/bao-cao-cuoi-ngay.component';
import { BaoCaoBanHangComponent } from './bao-cao-ban-hang/bao-cao-ban-hang.component';
import { BaoCaoHangHoaComponent } from './bao-cao-hang-hoa/bao-cao-hang-hoa.component';
//import {ChartsModule} from 'ng2-charts';
//import { FusionChartsModule } from 'angular2-fusioncharts';



@NgModule({
  declarations: [BaoCaoCuoiNgayComponent, BaoCaoBanHangComponent, BaoCaoHangHoaComponent],
  imports: [
    CommonModule,
    RouterModule,
   // ChartsModule
  ],
  // exports:[BaoCaoComponent],
  // schemas:[CUSTOM_ELEMENTS_SCHEMA]

})
export class BaoCaoModule { }
