import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes} from '@angular/router';
import { BaoCaoCuoiNgayComponent } from './bao-cao-cuoi-ngay/bao-cao-cuoi-ngay.component';
import { BaoCaoBanHangComponent } from './bao-cao-ban-hang/bao-cao-ban-hang.component';
import { BaoCaoHangHoaComponent } from './bao-cao-hang-hoa/bao-cao-hang-hoa.component';

const routes: Routes = [
 // {path: '', redirectTo: '/baocao/cuoingay', pathMatch: 'full'},
  // {path: '', component: BaoCaoComponent},
   {path: 'cuoingay', component: BaoCaoCuoiNgayComponent},
 {path: 'banhang', component: BaoCaoBanHangComponent},
 {path: 'hanghoa', component: BaoCaoHangHoaComponent}
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class BaoCaoRoutingModule { }
