import { Component, OnInit } from '@angular/core';
import { DanhMucNVLModel } from 'src/app/master/shared/models/danh-muc-nvl.model';
import { DanhMucNVLServiceService } from 'src/app/master/shared/services/danh-muc-nvlservice.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs';
Router

@Component({
  selector: 'app-danh-muc-nvl-edit',
  templateUrl: './danh-muc-nvl-edit.component.html',
  styleUrls: ['./danh-muc-nvl-edit.component.css']
})
export class DanhMucNVLEditComponent implements OnInit {
  public nguyenlieu : DanhMucNVLModel;
  constructor(
    private NVLservice: DanhMucNVLServiceService,
    public activatedRouteService: ActivatedRoute

    ) { }

  ngOnInit(){
    this.nguyenlieu = new DanhMucNVLModel();
    this.loadData();
  }
  loadData(){
    this.activatedRouteService.params.subscribe((data: Params) => {
      let maNVL = data['maNVL']
      this.NVLservice.get(maNVL).subscribe((nguyenlieu : DanhMucNVLModel)=> {
        this.nguyenlieu =nguyenlieu;
      });
    });
  }
//  onEdit(){
    
//   }
  // onEdit(){
  //   this.NVLservice.put(this.nguyenlieu.maNVL,).subscribe((data: DanhMucNVLModel)=> {
  //     console.log(data);
  //   })
  // }
  onEdit(){
    this.NVLservice.patch(this.nguyenlieu.maNVL, this.nguyenlieu).subscribe(res => {
      this.nguyenlieu=res;
    })
  }
}
