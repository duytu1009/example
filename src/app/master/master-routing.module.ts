import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MasterComponent } from './master.component';
import { TongQuanComponent } from './tong-quan/tong-quan.component';
import { BaoCaoRoutingModule } from './bao-cao/bao-cao-routing.module';
import { HangHoaRoutingModule } from './hang-hoa/hang-hoa-routing.module';
import { BanHangComponent } from './ban-hang/ban-hang.component';

const routes: Routes = [
  {
    path: '', component: MasterComponent, children: [
      {path: '', redirectTo: 'tongquan', pathMatch: 'full'},
      {path: 'tongquan', component: TongQuanComponent},
      
        {
           path: 'hanghoa',
           loadChildren: () => import('./hang-hoa/hang-hoa.module').then(m => m.HangHoaModule)
         },
         {
           path: 'baocao',
           loadChildren: () => import('./bao-cao/bao-cao.module').then(m => m.BaoCaoModule)
         },
         {path: 'banhanghoa', component: BanHangComponent},


    ]
  }
];
@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
   BaoCaoRoutingModule,
   HangHoaRoutingModule
  ],
  exports: [RouterModule]
})
export class MasterRoutingModule { }
