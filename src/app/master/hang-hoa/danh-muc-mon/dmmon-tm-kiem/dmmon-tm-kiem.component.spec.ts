import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DMMonTmKiemComponent } from './dmmon-tm-kiem.component';

describe('DMMonTmKiemComponent', () => {
  let component: DMMonTmKiemComponent;
  let fixture: ComponentFixture<DMMonTmKiemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DMMonTmKiemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DMMonTmKiemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
