import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DanhMucMonAddComponent } from './danh-muc-mon/danh-muc-mon-add/danh-muc-mon-add.component';
import { DanhMucMonListComponent } from './danh-muc-mon/danh-muc-mon-list/danh-muc-mon-list.component';
import { DanhMucNVLListComponent } from './danh-muc-nvl/danh-muc-nvl-list/danh-muc-nvl-list.component';
import { DanhMucNVLAddComponent } from './danh-muc-nvl/danh-muc-nvl-add/danh-muc-nvl-add.component';
import { RouterModule } from '@angular/router';
import { HangHoaRoutingModule } from './hang-hoa-routing.module';
import { MasterRoutingModule } from '../master-routing.module';
import { ThietLapGiaListComponent } from './thiet-lap-gia/thiet-lap-gia-list/thiet-lap-gia-list.component';
import { DMMonTmKiemComponent } from './danh-muc-mon/dmmon-tm-kiem/dmmon-tm-kiem.component';
import { FormsModule } from '@angular/forms';
import { DanhMucNVLEditComponent } from './danh-muc-nvl/danh-muc-nvl-edit/danh-muc-nvl-edit.component';
import { DanhMucMonEditComponent } from './danh-muc-mon/danh-muc-mon-edit/danh-muc-mon-edit.component';
import { ExcelServiceService } from '../shared/services/excel-service.service';
//import { Ng2SearchPipeModule } from 'ng2-search-filter';
//import { Pipe, PipeTransform } from '@angular/core';


@NgModule({
  declarations: [DanhMucMonAddComponent, DanhMucMonListComponent, DanhMucNVLListComponent, DanhMucNVLAddComponent, ThietLapGiaListComponent, DMMonTmKiemComponent, DanhMucNVLEditComponent, DanhMucMonEditComponent],
  imports: [
    CommonModule,
    RouterModule,
    MasterRoutingModule,
    HangHoaRoutingModule,
    FormsModule,
   

  ],
  providers:[ExcelServiceService]
})
export class HangHoaModule { }
