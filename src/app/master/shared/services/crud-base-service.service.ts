import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';
import { RequestQueryBuilder, CreateQueryParams } from '@nestjsx/crud-request';


// @Injectable({
//   providedIn: 'root'
// })
export interface CrudBaseOptions {
  entity: string;
  apiUrl: string;
  // params?: ParamsOptions
}
export type ParamOptionType = 'number' | 'string' | 'uuid';


export abstract class CrudBaseServiceService<T> {
  protected options: CrudBaseOptions;
  constructor(options: CrudBaseOptions,
    protected http: HttpClient) {
    this.options = options
  }


  getAll(builder?: RequestQueryBuilder | CreateQueryParams): Observable<T[]> {
    if (!(builder instanceof RequestQueryBuilder)) {
      builder = RequestQueryBuilder.create(builder);
    }
    return this.http.get<T[]>(this.getBaseUrl(), {
      params: this.getParamsFromQuery(builder)
    });
  }
  get(value: number | string | boolean, builder?: RequestQueryBuilder | CreateQueryParams): Observable<T> {
    const url = this.getOneUrl(value);
    if (!(builder instanceof RequestQueryBuilder)) {
      builder = RequestQueryBuilder.create(builder);
    }
    return this.http.get<T>(url, {
      params: this.getParamsFromQuery(builder)
    });
  }

  create(body: T): Observable<T> {
    return this.http.post<T>(this.getBaseUrl(), JSON.stringify(body), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  creates(body: T[]): Observable<T[]> {
    return this.http.post<T[]>(this.getBaseUrl() + '/bulk',
      JSON.stringify({ bulk: body }),
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }

  put(value: number | string | boolean, body: T): Observable<T> {
    const url = this.getOneUrl(value);
    return this.http.put<T>(url, JSON.stringify(body), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  patch(value: number | string | boolean, body: T): Observable<T> {
    const url = this.getOneUrl(value);
    return this.http.patch<T>(url, JSON.stringify(body), {
      headers: {
        'Content-Type': 'application/json'
      }
    });
  }

  delete(value: number | string | boolean): Observable<void> {
    const url = this.getOneUrl(value);
    return this.http.delete<void>(url);
  }

  protected getParamsFromQuery(builder: RequestQueryBuilder) {
    return new HttpParams({ fromString: this.getQuery(builder) });
  }

  /**
     * Get request link
     */
  protected getBaseUrl(): string {
    return `${this.options.apiUrl}/${this.options.entity}`;
  }

  protected getOneUrl(value: number | string | boolean) {
    return `${this.getBaseUrl()}/${value}`;
  }

  private getQuery(builder?: RequestQueryBuilder) {
    if (builder) {
      return builder.query();
    }
    return '';
  }
}