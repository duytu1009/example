import { Injectable } from '@angular/core';
import { CrudBaseServiceService } from './crud-base-service.service';
import { DanhMucNVLModel } from '../models/danh-muc-nvl.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DanhMucNVLServiceService extends CrudBaseServiceService<DanhMucNVLModel>{

  constructor(http: HttpClient) { 
    super({
          apiUrl: environment.apiUrl,
           entity: 'nguyen-vat-lieu'
         }, http);
  }
}
