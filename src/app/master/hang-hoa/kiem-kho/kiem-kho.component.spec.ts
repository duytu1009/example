import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { KiemKhoComponent } from './kiem-kho.component';

describe('KiemKhoComponent', () => {
  let component: KiemKhoComponent;
  let fixture: ComponentFixture<KiemKhoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ KiemKhoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(KiemKhoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
