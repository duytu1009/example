import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThietLapGiaListComponent } from './thiet-lap-gia-list.component';

describe('ThietLapGiaListComponent', () => {
  let component: ThietLapGiaListComponent;
  let fixture: ComponentFixture<ThietLapGiaListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThietLapGiaListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThietLapGiaListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
