import { Injectable } from '@angular/core';
import { CrudBaseServiceService } from './crud-base-service.service';
import { DanhMucMonModel } from '../models/danh-muc-mon.model';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})

export class DanhMucMonService extends CrudBaseServiceService<DanhMucMonModel>{

  constructor(http: HttpClient) {
    super({
      apiUrl: environment.apiUrl,
      entity: 'mon'
    }, http);
  }
}
