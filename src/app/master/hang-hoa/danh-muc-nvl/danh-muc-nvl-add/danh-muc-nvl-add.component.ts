import { Component, OnInit } from '@angular/core';
import { DanhMucNVLModel } from 'src/app/master/shared/models/danh-muc-nvl.model';
import { DanhMucNVLServiceService } from 'src/app/master/shared/services/danh-muc-nvlservice.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-danh-muc-nvl-add',
  templateUrl: './danh-muc-nvl-add.component.html',
  styleUrls: ['./danh-muc-nvl-add.component.css']
})
export class DanhMucNVLAddComponent implements OnInit {
  public nguyenlieu: DanhMucNVLModel;
  
  constructor(private service: DanhMucNVLServiceService,
    public routerService : Router
  ) { }

  ngOnInit(): void {
    this.nguyenlieu = new DanhMucNVLModel();
    this.onAddNguyenLieu();
  }
  onAddNguyenLieu() {
    this.service.create(this.nguyenlieu).subscribe(res => {
     // console.log(res);
     this.nguyenlieu=res;
     //console.log(this.nguyenlieu) 
     alert("thêm thành công");
    // this.routerService.navigate(['/hanghoa/danhmucNVL/list'])
    })
}
}