import { Component, OnInit } from '@angular/core';
import { RequestQueryBuilder } from '@nestjsx/crud-request';
import { DanhMucNVLModel } from 'src/app/master/shared/models/danh-muc-nvl.model';
import { DanhMucNVLServiceService } from 'src/app/master/shared/services/danh-muc-nvlservice.service';
import { ExcelServiceService } from 'src/app/master/shared/services/excel-service.service';

@Component({
  selector: 'app-danh-muc-nvl-list',
  templateUrl: './danh-muc-nvl-list.component.html',
  styleUrls: ['./danh-muc-nvl-list.component.css']
})
export class DanhMucNVLListComponent implements OnInit {
  filterStatus = 'XEM_TAT_CA';
  locNguyenLieu='TAT_CA';
 
  // lstNguyenLieu: Array<DanhMucNVLModel> = [];
  constructor(private service: DanhMucNVLServiceService,
    private excelService: ExcelServiceService) { }
    tenNVL: string;

  lstNguyenLieu: Array<DanhMucNVLModel>=[
    {
      maNVL: 1,
      maNCC:1,
      maLoai:1,
      tenNVL: 'Trà đen',
      DVT: 'kg',
      giaNVL: 100000,
      tonKho: 10
    },
    {
      maNVL: 2,
      maNCC:2,
      maLoai:3,
      tenNVL: 'Mứt xoài',
      DVT: 'chai',
      giaNVL: 180000,
      tonKho: 0,
    },
    {
      maNVL: 3,
      maNCC:2,
      maLoai:2,
      tenNVL: 'Bột phô mai',
      DVT: 'bì',
      giaNVL: 200000,
      tonKho: 2,
    },
    {
      maNVL: 4,
      maNCC:1,
      maLoai:4,
      tenNVL: 'Syrup dưa hấu',
      DVT: 'chai',
      giaNVL: 110000,
      tonKho: 11,
    },
    {
      maNVL: 5,
      maNCC:2,
      maLoai:2,
      tenNVL: 'Bột trà xanh',
      DVT: 'bì',
      giaNVL: 170000,
      tonKho: 6,
    },
    {
      maNVL: 6,
      maNCC:2,
      maLoai:2,
      tenNVL: 'Bột rau cau',
      DVT: 'bì',
      giaNVL: 150000,
      tonKho: 0,
    },
    {
      maNVL: 7,
      maNCC:1,
      maLoai:4,
      tenNVL: 'Syrup dau',
      DVT: 'chai',
      giaNVL: 110000,
      tonKho: 0,
    },
  ]

  ngOnInit() {
    this.loadNguyenLieu();
  // this.xuatFile();
 // this.Search();
  }

  loadNguyenLieu() {
    this.service.getAll(RequestQueryBuilder.create({
      fields: ['maNVL', 'maNCC', 'maLoai', 'tenNVL', 'DVT', 'giaNVL', 'tonKho'],
    })).subscribe(res => {
      this.lstNguyenLieu = res;
      console.log(this.lstNguyenLieu);
    });
  }
  onDelete(maNVL: number){
    let confirmResult = confirm("Bạn có chắc chắn muốn xóa ?");
    if(confirmResult){
      this.service.delete(maNVL).subscribe(data => {
        console.log(data);
        this.loadNguyenLieu();
    })
  }
}
 // onDelete(){}
  getShowStatus(tonKho: number){
    const dkXemTatCa = this.filterStatus === 'XEM_TAT_CA';
    const dkConHang = this.filterStatus === 'CON_HANG' && tonKho > 0;
    const dkHetHang = this.filterStatus === 'HET_HANG' && tonKho == 0;
    return dkXemTatCa || dkConHang || dkHetHang;
  }
  getShowNguyenLieu(maLoai: number){
    const dkTatCa = this.locNguyenLieu === 'TAT_CA';
    const dkTra = this.locNguyenLieu === 'TRA' && maLoai == 1;
    const dkBot = this.locNguyenLieu === 'BOT' && maLoai == 2;
    const dkMut = this.locNguyenLieu === 'MUT' && maLoai == 3;
    const dkSyrup = this.locNguyenLieu === 'SYRUP' && maLoai == 4;
    const dkDuong = this.locNguyenLieu === 'DUONG' && maLoai == 5;
    const dktranchau = this.locNguyenLieu === 'TRANCHAU' && maLoai == 6;
    const dkSuadac = this.locNguyenLieu === 'SUADAC' && maLoai == 7;

    return dkTatCa || dkTra || dkBot || dkMut || dkSyrup|| dkDuong||dktranchau||dkSuadac;
  }
  xuatFile(){
    this.excelService.exportAsExcelFile(this.lstNguyenLieu, 'DS NVL');
  }
  Search(){
    if(this.tenNVL != ''){
      this.lstNguyenLieu= this.lstNguyenLieu.filter(res=>{
        return res.tenNVL.toLocaleLowerCase(this.tenNVL).match(this.tenNVL.toLocaleLowerCase());
      });
    }else if(this.tenNVL == ''){
      this.ngOnInit();
    }
   
  }
}
