import { Component, OnInit } from '@angular/core';
import { DanhMucMonModel } from 'src/app/master/shared/models/danh-muc-mon.model';
import { DanhMucMonService } from 'src/app/master/shared/services/danh-muc-mon.service';
import { RequestQueryBuilder } from '@nestjsx/crud-request';


@Component({
  selector: 'app-danh-muc-mon-list',
  templateUrl: './danh-muc-mon-list.component.html',
  styleUrls: ['./danh-muc-mon-list.component.css']
})
export class DanhMucMonListComponent implements OnInit {
  locMon='TAT_CA';
  constructor(
    private service:DanhMucMonService 
  ) { }
  lstMon: Array<DanhMucMonModel> = [
    {
      maMon: 1,
      tenMon:'Trà đào',
      maLoai:3,
      donGia: 25000

    },
    {
      maMon: 2,
      tenMon:'Trà xanh đá xay',
      maLoai:4,
      donGia: 35000
    },
    {
      maMon: 3,
      tenMon:'Trà sữa trà đen',
      maLoai:1,
      donGia: 28000
    }
  ];
  ngOnInit(): void {
    this.loadMon(); 
  }
  loadMon(){
    this.service.getAll(RequestQueryBuilder.create({
      fields:['maMon','maLoai','tenMon','donGia'],
    })).subscribe(res => {
      this.lstMon =res;
      console.log(this.lstMon)
    })
  }
  getShowMon(maLoai: number){
    const dkTatCa = this.locMon === 'TAT_CA';
    const dkTraSua = this.locMon === 'TRASUA' && maLoai == 1;
    const dkTraVS = this.locMon === 'TRAVS' && maLoai == 2;
    const dkTraTraiCay = this.locMon === 'TRATRAICAY' && maLoai == 3;
    const dkTraDX = this.locMon === 'TRADAXAY' && maLoai == 4;
    const dktopping = this.locMon === 'TOPPING' && maLoai == 5;

    return dkTatCa || dkTraSua || dkTraVS || dkTraTraiCay || dkTraDX|| dktopping;
  }

}
