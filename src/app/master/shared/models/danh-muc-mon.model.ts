export class DanhMucMonModel {
    maMon: number;
    maLoai: number;
    tenMon: string;
    donGia: number;
}